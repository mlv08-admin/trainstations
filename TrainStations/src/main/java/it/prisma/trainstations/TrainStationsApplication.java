package it.prisma.trainstations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainStationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainStationsApplication.class, args);
    }

}
